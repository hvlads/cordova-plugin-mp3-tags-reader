var exec = require('cordova/exec');

exports.reedMp3 = function(arg0, success, error) {
    exec(success, error, "cordova-plugin-reed-mp3-tags", "reedMp3", [arg0]);
};
