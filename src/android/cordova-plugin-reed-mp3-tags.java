package ru.hvlads.readmp3tags;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.media.MediaMetadataRetriever;

/**
 * This class echoes a string called from JavaScript.
 */
public class cordova-plugin-reed-mp3-tags extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("reedMp3")) {
            String filePath = args.getString(0);
            this.reedMp3(message, callbackContext);
            return true;
        }
        return false;
    }

    private void reedMp3(String filePath, CallbackContext callbackContext) {
        if (filePath != null && filePath.length() > 0) {
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(filePath);
            String album = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
            String artist = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
            String genre = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE);
            String title = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
            byte[] image = mmr.getEmbeddedPicture();
            JSONObject tags = new JSONObject();
            tags.put("album", album);
            tags.put("artist", artist);
            tags.put("genre", genre);
            tags.put("title", title);
            tags.put("image", image);
            callbackContext.success(tags);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }
}
